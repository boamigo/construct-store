ConstructStore.Web - this folder contains Mvc Core web application
Test.ConstructStore.Web - this is UnitTest project for the ConstructStore.Web
frontend - this is Vue frontend application, which is pure html/js and has no dotnet in it

Open the SLN file in VS or Rider and Run the application.
Open the browser and navigate to http://localhost:5000.

Notes:
   The frontend VueJS application is compiled into the wwwroot folder of the Mvc core web application - so you don't have to install and build it.

Important:
   I used JetBrains Rider and VScode on my Debian 9 at home. I committed code to bitbucker and succeeded to restore it on my Debian 9 at work.
   I don't know how it will go on Windows in Visual Studio.
   Since I am not using any MQ - I find it redundant having an additional Mvc core application just to serve the frontend app.
   Instead I can deploy the frontend html/js code to any webserver and configure proxy at the webserver having all calls coming
   from the frontend browser app to be redirected to my MVC core application which resides in my network behind firewalls.

Thank you for your time!
