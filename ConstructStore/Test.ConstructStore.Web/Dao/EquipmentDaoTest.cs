using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao.Database;
using ConstructStore.Web.Dao.Impl;
using ConstructStore.Web.Domain;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Dao
{
    public class EquipmentDaoTest
    {
        private Mock<IDatabase> database;
        private EquipmentDaoImpl target;

        [SetUp]
        public void Setup()
        {
            database = new Mock<IDatabase>();
            target = new EquipmentDaoImpl { Database = database.Object };
        }

        [Test]
        public void GetAllTest()
        {
            IList<Equipment> items = new List<Equipment>();
            database.Setup(o => o.ExecuteQuery<IList<Equipment>>(QueryType.AllEquipmentsWithLang, "LANG"))
                .Returns(Task.FromResult(items));
            var actual = target.GetAll("LANG").Result;
            Assert.AreSame(items, actual);
        }

        [Test]
        public void GetByCodeTest()
        {
            var item = new Equipment();
            database.Setup(o => o.ExecuteQuery<Equipment>(QueryType.EquipmentByCodeWithLang, "CODE", "LANG"))
                .Returns(Task.FromResult(item));
            var actual = target.GetByCode("CODE", "LANG").Result;
            Assert.AreSame(item, actual);
        }
    }
}
