using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao.Database;
using ConstructStore.Web.Dao.Impl;
using ConstructStore.Web.Domain;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Dao
{
    public class EquipmentTypeDaoTest
    {
        private Mock<IDatabase> database;
        private EquipmentTypeDaoImpl target;

        [SetUp]
        public void Setup()
        {
            database = new Mock<IDatabase>();
            target = new EquipmentTypeDaoImpl { Database = database.Object };
        }

        [Test]
        public void GetAllTest()
        {
            IList<EquipmentType> items = new List<EquipmentType>();
            database.Setup(o => o.ExecuteQuery<IList<EquipmentType>>(QueryType.AllEquipmentTypes))
                .Returns(Task.FromResult(items));
            var actual = target.GetAll().Result;
            Assert.AreSame(items, actual);
        }
    }
}