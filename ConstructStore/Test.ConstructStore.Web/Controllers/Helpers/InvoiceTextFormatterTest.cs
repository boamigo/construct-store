using System.Collections.Generic;
using ConstructStore.Web.Controllers.Helpers.Impl;
using ConstructStore.Web.Services.Dto;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Controllers.Helpers
{
    public class InvoiceTextFormatterTest
    {
        private InvoiceTextFormatterImpl target;

        [SetUp]
        public void Setup()
        {
            target = new InvoiceTextFormatterImpl();
        }

        [Test]
        public void GetAllTest()
        {
            var invoice = new InvoiceData
            {
                Title = "TITLE - TITLE",
                Lines = new List<InvoiceLine>
                {
                    new InvoiceLine {Bonus = 111, Fee = 222, DaysToRent = 333, Name = "AAA"},
                    new InvoiceLine {Bonus = 444, Fee = 555, DaysToRent = 666, Name = "BBB"}
                }
            };
            var expected = "TITLE - TITLE\n\n" +
                "days: 333\tbonus: 111\tfee: 222\t\tAAA\n" +
                "days: 666\tbonus: 444\tfee: 555\t\tBBB\n\n" +
                "Totals:\n" +
                "\tFee: 777\n" +
                "\tBonus: 555\n";
            var actual = target.FormatInvoice(invoice);
            Assert.AreEqual(expected, actual);
        }
    }
}
