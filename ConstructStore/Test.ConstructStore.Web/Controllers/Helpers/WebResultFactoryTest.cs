using System.Text;
using ConstructStore.Web.Controllers.Helpers.Impl;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Controllers.Helpers
{
    public class WebResultFactoryTest
    {
        private WebResultFactoryImpl target;

        [SetUp]
        public void Setup()
        {
            target = new WebResultFactoryImpl();
        }

        [Test]
        public void ConvertTextToFileTest()
        {
            const string text = "HELLO WORLD !!!";
            var actual = (FileContentResult)target.ConvertTextToFile(text);
            Assert.NotNull(actual);
            Assert.AreEqual(Encoding.UTF8.GetBytes(text), actual.FileContents);
            Assert.AreEqual("text/plain", actual.ContentType);
        }
    }
}
