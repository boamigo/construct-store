using System.Threading.Tasks;
using ConstructStore.Web.Controllers;
using ConstructStore.Web.Controllers.Helpers;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Controllers
{
    public class InvoiceControllerTest
    {
        private Mock<IInvoiceService> invoiceService;
        private Mock<IInvoiceTextFormatter> invoiceTextFormatter;
        private Mock<IWebResultFactory> webResultFactory;
        private InvoiceController target;

        [SetUp]
        public void Setup()
        {
            invoiceService = new Mock<IInvoiceService>();
            invoiceTextFormatter = new Mock<IInvoiceTextFormatter>();
            webResultFactory = new Mock<IWebResultFactory>();
            target = new InvoiceController(invoiceService.Object, invoiceTextFormatter.Object, webResultFactory.Object);
        }

        [Test]
        public void GenerateTest()
        {
            var request = new GenerateInvoiceRequest();
            var generatedInvoice = new InvoiceData();
            const string formattedInvoiceText = "FORMATTED_INVOICE";
            var resultFile = new Mock<IActionResult>();
            invoiceService.Setup(o => o.GenerateInvoice(request)).Returns(Task.FromResult(generatedInvoice));
            invoiceTextFormatter.Setup(o => o.FormatInvoice(generatedInvoice)).Returns(formattedInvoiceText);
            webResultFactory.Setup(o => o.ConvertTextToFile(formattedInvoiceText)).Returns(resultFile.Object);
            var actual = target.Generate(request).Result;
            Assert.AreSame(resultFile.Object, actual);
            invoiceService.VerifyAll();
            invoiceTextFormatter.VerifyAll();
            webResultFactory.VerifyAll();
        }
    }
}
