using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Controllers;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Controllers
{
    public class EquipmentTypeControllerTest
    {
        private Mock<ILookupService> lookupService;
        private EquipmentTypeController target;

        [SetUp]
        public void Setup()
        {
            lookupService = new Mock<ILookupService>();
            target = new EquipmentTypeController(lookupService.Object);
        }

        [Test]
        public void GetAllTest()
        {
            IList<EquipmentTypeDto> items = new List<EquipmentTypeDto>();
            lookupService.Setup(o => o.GetEquipmentTypes()).Returns(Task.FromResult(items));
            var actual = target.Get().Result;
            Assert.AreSame(items, actual);
        }
    }
}
