using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Controllers;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Controllers
{
    public class EquipmentControllerTest
    {
        private Mock<IEquipmentService> equipmentService;
        private EquipmentController target;

        [SetUp]
        public void Setup()
        {
            equipmentService = new Mock<IEquipmentService>();
            target = new EquipmentController(equipmentService.Object);
        }

        [Test]
        public void GetAllTest()
        {
            IList<EquipmentDto> items = new List<EquipmentDto>();
            equipmentService.Setup(o => o.GetAll("LANG")).Returns(Task.FromResult(items));
            var actual = target.GetAll("LANG").Result;
            Assert.AreSame(items, actual);
        }

        [Test]
        public void GetByCodeTest()
        {
            var item = new EquipmentDto();
            equipmentService.Setup(o => o.GetByCode("CODE", "LANG")).Returns(Task.FromResult(item));
            var actual = target.GetByCode("CODE", "LANG").Result;
            Assert.AreSame(item, actual);
        }
    }
}
