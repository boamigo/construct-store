using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Impl;
using ConstructStore.Web.Services.Mapping;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Services
{
    public class EquipmentServiceTest
    {
        private Mock<IEquipmentDao> equipmentDao;
        private Mock<IEquipmentMapper> equipmentMapper;
        private EquipmentServiceImpl target;

        [SetUp]
        public void Setup()
        {
            equipmentDao = new Mock<IEquipmentDao>();
            equipmentMapper = new Mock<IEquipmentMapper>();
            target = new EquipmentServiceImpl
            {
                EquipmentDao = equipmentDao.Object,
                EquipmentMapper = equipmentMapper.Object
            };
        }

        [Test]
        public void GetAllTest()
        {
            IList<Equipment> domainItems = new List<Equipment>();
            var dtoItems = new List<EquipmentDto>();
            equipmentDao.Setup(o => o.GetAll("LANG")).Returns(Task.FromResult(domainItems));
            equipmentMapper.Setup(o => o.ToDto(domainItems)).Returns(dtoItems);
            var actual = target.GetAll("LANG").Result;
            Assert.AreSame(dtoItems, actual);
        }

        [Test]
        public void GetByCodeTest()
        {
            var domain = new Equipment();
            var dto = new EquipmentDto();
            equipmentDao.Setup(o => o.GetByCode("CODE", "LANG")).Returns(Task.FromResult(domain));
            equipmentMapper.Setup(o => o.ToDto(domain)).Returns(dto);
            var actual = target.GetByCode("CODE", "LANG").Result;
            Assert.AreSame(dto, actual);
        }
    }
}