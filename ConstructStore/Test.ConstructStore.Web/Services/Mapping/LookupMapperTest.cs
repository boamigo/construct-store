using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Mapping.Impl;
using NUnit.Framework;
using System.Collections.Generic;

namespace Test.ConstructStore.Web.Services.Mapping
{
    public class LookupMapperTest
    {
        private LookupMapperImpl target;

        [SetUp]
        public void Setup()
        {
            target = new LookupMapperImpl();
        }

        [Test]
        public void ToDtoListTest()
        {
            var item = new EquipmentType
            {
                Code = "CODE",
                Name = "NAME",
            };
            var actualItems = target.ToDto(new List<EquipmentType>() {item});
            Assert.AreEqual(1, actualItems.Count);
            var actual = actualItems[0];
            Assert.AreEqual("CODE", actual.Code);
            Assert.AreEqual("NAME", actual.Name);
        }
    }
}
