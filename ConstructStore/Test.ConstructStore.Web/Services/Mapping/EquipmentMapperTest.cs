using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Mapping.Impl;
using NUnit.Framework;
using System.Collections.Generic;

namespace Test.ConstructStore.Web.Services.Mapping
{
    public class EquipmentMapperTest
    {
        private EquipmentMapperImpl target;

        [SetUp]
        public void Setup()
        {
            target = new EquipmentMapperImpl();
        }

        [Test]
        public void ToDtoListTest()
        {
            var item = new Equipment
            {
                Code = "CODE",
                TypeCode = "TYPE_CODE",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n {LangCode = "LANG_CODE", Name = "LOCAL_NAME"}
                }
            };
            var actualItems = target.ToDto(new List<Equipment>() {item});
            Assert.AreEqual(1, actualItems.Count);
            var actual = actualItems[0];
            Assert.AreEqual("CODE", actual.Code);
            Assert.AreEqual("TYPE_CODE", actual.TypeCode);
            Assert.AreEqual("LOCAL_NAME", actual.Name);
        }

        [Test]
        public void ToDtoTest()
        {
            var item = new Equipment
            {
                Code = "CODE",
                TypeCode = "TYPE_CODE",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n {LangCode = "LANG_CODE", Name = "LOCAL_NAME"}
                }
            };
            var actual = target.ToDto(item);
            Assert.AreEqual("CODE", actual.Code);
            Assert.AreEqual("TYPE_CODE", actual.TypeCode);
            Assert.AreEqual("LOCAL_NAME", actual.Name);
        }
    }
}
