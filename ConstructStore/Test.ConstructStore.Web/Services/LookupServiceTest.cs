using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Impl;
using ConstructStore.Web.Services.Mapping;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Services
{
    public class LookupServiceTest
    {
        private Mock<IEquipmentTypeDao> equipmentTypeDao;
        private Mock<ILookupMapper> lookupMapper;
        private LookupServiceImpl target;

        [SetUp]
        public void Setup()
        {
            equipmentTypeDao = new Mock<IEquipmentTypeDao>();
            lookupMapper = new Mock<ILookupMapper>();
            target = new LookupServiceImpl
            {
                EquipmentTypeDao = equipmentTypeDao.Object,
                LookupMapper = lookupMapper.Object
            };
        }

        [Test]
        public void GetAllTest()
        {
            IList<EquipmentType> domainItems = new List<EquipmentType>();
            var dtoItems = new List<EquipmentTypeDto>();
            equipmentTypeDao.Setup(o => o.GetAll()).Returns(Task.FromResult(domainItems));
            lookupMapper.Setup(o => o.ToDto(domainItems)).Returns(dtoItems);
            var actual = target.GetEquipmentTypes().Result;
            Assert.AreSame(dtoItems, actual);
        }
    }
}
