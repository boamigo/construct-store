using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Helpers;
using ConstructStore.Web.Services.Impl;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Services
{
    public class InvoiceServiceTest
    {
        private Mock<IEquipmentDao> equipmentDao;
        private Mock<IInvoiceFactory> invoiceFactory;
        private InvoiceServiceImpl target;

        [SetUp]
        public void Setup()
        {
            equipmentDao = new Mock<IEquipmentDao>();
            invoiceFactory = new Mock<IInvoiceFactory>();
            target = new InvoiceServiceImpl
            {
                EquipmentDao = equipmentDao.Object,
                InvoiceFactory = invoiceFactory.Object
            };
        }

        [Test]
        public void GenerateInvoiceTest()
        {
            var invoiceItem = new InvoiceItem {EquipmentCode = "CODE"};
            var request = new GenerateInvoiceRequest
            {
                Lang = "LANG",
                Items = new List<InvoiceItem> {invoiceItem}
            };
            var invoice = new InvoiceData();
            var equipment = new Equipment();
            invoiceFactory.Setup(o => o.CreateEmptyInvoice(request)).Returns(invoice);
            equipmentDao.Setup(o => o.GetByCode("CODE", "LANG")).Returns(Task.FromResult(equipment));
            invoiceFactory.Setup(o => o.AppendInvoiceLine(invoice, equipment, invoiceItem));
            var actual = target.GenerateInvoice(request).Result;
            
            Assert.AreSame(invoice, actual);
            equipmentDao.VerifyAll();
            invoiceFactory.VerifyAll();
        }
    }
}
