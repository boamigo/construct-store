using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Helpers.Impl;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Services.Helpers
{
    public class FeeCalcTest
    {
        private FeeCalcImpl target;

        [SetUp]
        public void Setup()
        {
            target = new FeeCalcImpl();
        }

        [Test]
        public void Calculate_Heavy_1_day()
        {
            var equipment = new Equipment {TypeCode = "HVY"};
            var actual = target.CalculateFee(equipment, 1);
            Assert.AreEqual(160, actual);
        }

        [Test]
        public void Calculate_Heavy_2_day()
        {
            var equipment = new Equipment {TypeCode = "HVY"};
            var actual = target.CalculateFee(equipment, 2);
            Assert.AreEqual(220, actual);
        }

        [Test]
        public void Calculate_Heavy_3_day()
        {
            var equipment = new Equipment {TypeCode = "HVY"};
            var actual = target.CalculateFee(equipment, 3);
            Assert.AreEqual(280, actual);
        }

        [Test]
        public void Calculate_Heavy_20_day()
        {
            var equipment = new Equipment {TypeCode = "HVY"};
            var actual = target.CalculateFee(equipment, 20);
            Assert.AreEqual(1300, actual);
        }

        [Test]
        public void Calculate_Regular_1_day()
        {
            var equipment = new Equipment {TypeCode = "RGR"};
            var actual = target.CalculateFee(equipment, 1);
            Assert.AreEqual(160, actual);
        }

        [Test]
        public void Calculate_Regular_2_day()
        {
            var equipment = new Equipment {TypeCode = "RGR"};
            var actual = target.CalculateFee(equipment, 2);
            Assert.AreEqual(220, actual);
        }

        [Test]
        public void Calculate_Regular_3_day()
        {
            var equipment = new Equipment {TypeCode = "RGR"};
            var actual = target.CalculateFee(equipment, 3);
            Assert.AreEqual(260, actual);
        }

        [Test]
        public void Calculate_Regular_20_day()
        {
            var equipment = new Equipment {TypeCode = "RGR"};
            var actual = target.CalculateFee(equipment, 20);
            Assert.AreEqual(940, actual);
        }
        
        [Test]
        public void Calculate_Specialized_1_day()
        {
            var equipment = new Equipment {TypeCode = "SPZ"};
            var actual = target.CalculateFee(equipment, 1);
            Assert.AreEqual(60, actual);
        }
        
        [Test]
        public void Calculate_Specialized_2_day()
        {
            var equipment = new Equipment {TypeCode = "SPZ"};
            var actual = target.CalculateFee(equipment, 2);
            Assert.AreEqual(120, actual);
        }
        
        [Test]
        public void Calculate_Specialized_3_day()
        {
            var equipment = new Equipment {TypeCode = "SPZ"};
            var actual = target.CalculateFee(equipment, 3);
            Assert.AreEqual(180, actual);
        }
        
        [Test]
        public void Calculate_Specialized_4_day()
        {
            var equipment = new Equipment {TypeCode = "SPZ"};
            var actual = target.CalculateFee(equipment, 4);
            Assert.AreEqual(220, actual);
        }
        
        [Test]
        public void Calculate_Specialized_20_day()
        {
            var equipment = new Equipment {TypeCode = "SPZ"};
            var actual = target.CalculateFee(equipment, 20);
            Assert.AreEqual(860, actual);
        }
    }
}
