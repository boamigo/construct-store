using System.Collections.Generic;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Helpers;
using ConstructStore.Web.Services.Helpers.Impl;
using Moq;
using NUnit.Framework;

namespace Test.ConstructStore.Web.Services.Helpers
{
    public class InvoiceFactoryTest
    {
        private Mock<IFeeCalc> feeCalc;
        private InvoiceFactoryImpl target;

        [SetUp]
        public void Setup()
        {
            feeCalc = new Mock<IFeeCalc>();
            target = new InvoiceFactoryImpl {FeeCalc = feeCalc.Object};
        }

        [Test]
        public void CreateEmptyInvoiceTest()
        {
            var actual = target.CreateEmptyInvoice(null);
            Assert.NotNull(actual);
            Assert.AreEqual("Invoice Title", actual.Title);
            Assert.NotNull(actual.Lines);
            Assert.AreEqual(0, actual.Lines.Count);
        }

        [Test]
        public void AppendInvoiceLineTest()
        {
            var invoice = new InvoiceData {Lines = new List<InvoiceLine>()};
            var equipment = new Equipment
            {
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n
                    {
                        Name = "LOC_NAME"
                    }
                }
            };
            var item = new InvoiceItem {DaysToRent = 111};
            feeCalc.Setup(o => o.CalculateFee(equipment, 111)).Returns(555);
            feeCalc.Setup(o => o.CalculateBonus(equipment)).Returns(777);
            target.AppendInvoiceLine(invoice, equipment, item);
            Assert.AreEqual(1, invoice.Lines.Count);
            var line = invoice.Lines[0];
            Assert.AreEqual("LOC_NAME", line.Name);
            Assert.AreEqual(555, line.Fee);
            Assert.AreEqual(777, line.Bonus);
            Assert.AreEqual(111, line.DaysToRent);
        }
    }
}
