namespace ConstructStore.Web.Services.Dto
{
    public class EquipmentDto
    {
        public string Code { get; set; }
        public string TypeCode { get; set; }
        public string Name { get; set; }
    }
}
