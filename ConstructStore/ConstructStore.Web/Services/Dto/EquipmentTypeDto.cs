namespace ConstructStore.Web.Services.Dto
{
    public class EquipmentTypeDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
