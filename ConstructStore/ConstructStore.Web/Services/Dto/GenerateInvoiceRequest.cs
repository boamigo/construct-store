using System.Collections.Generic;

namespace ConstructStore.Web.Services.Dto
{
    public class GenerateInvoiceRequest
    {
        public  string Lang { get; set; }
        public IList<InvoiceItem> Items { get; set; }
    }

    public class InvoiceItem
    {
        public string EquipmentCode { get; set; }
        public int DaysToRent { get; set; }
    }
}
