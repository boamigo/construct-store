using System.Collections.Generic;

namespace ConstructStore.Web.Services.Dto
{
    public class InvoiceData
    {
        public string Title { get; set; }
        public IList<InvoiceLine> Lines { get; set; }
    }

    public class InvoiceLine
    {
        public string Name { get; set; }
        public int Fee { get; set; }
        public int Bonus { get; set; }
        public int DaysToRent { get; set; }
    }
}
