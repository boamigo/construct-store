using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services.Helpers
{
    public interface IFeeCalc
    {
        int CalculateFee(Equipment equipment, int daysToRent);
        int CalculateBonus(Equipment equipment);
    }
}