using System;
using ConstructStore.Web.Domain;

namespace ConstructStore.Web.Services.Helpers.Impl
{
    public class FeeCalcImpl: IFeeCalc
    {
        private const int ONETIME_FEE = 100;
        private const int PREMIUM_FEE = 60;
        private const int REGULAR_FEE = 40;
        
        public int CalculateFee(Equipment equipment, int daysToRent)
        {
            switch (equipment.TypeCode)
            {
                case "HVY":
                    return ONETIME_FEE + PREMIUM_FEE * daysToRent;
                case "RGR":
                    if (daysToRent <= 2) {
                        return ONETIME_FEE + PREMIUM_FEE * daysToRent;
                    }
                    else {
                        return ONETIME_FEE + PREMIUM_FEE * 2 + REGULAR_FEE * (daysToRent - 2);
                    }

                case "SPZ":
                    if (daysToRent <= 3) {
                        return PREMIUM_FEE * daysToRent;
                    }
                    else {
                        return PREMIUM_FEE * 3 + REGULAR_FEE * (daysToRent - 3);
                    }

                default:
                    throw new Exception("Unknown type code");
            }
        }

        public int CalculateBonus(Equipment equipment)
        {
            if (equipment.TypeCode == "HVY")
            {
                return 2;
            }
            
            return 1;
        }
    }
}
