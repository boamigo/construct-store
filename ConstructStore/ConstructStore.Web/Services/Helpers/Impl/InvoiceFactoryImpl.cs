using System.Collections.Generic;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services.Helpers.Impl
{
    public class InvoiceFactoryImpl: IInvoiceFactory
    {
        public IFeeCalc FeeCalc { get; set; }
        
        public InvoiceData CreateEmptyInvoice(GenerateInvoiceRequest request)
        {
            return new InvoiceData
            {
                Title = "Invoice Title", // supposed to be taken from localized resource
                Lines = new List<InvoiceLine>()
            };
        }

        public void AppendInvoiceLine(InvoiceData invoice, Equipment equipment, InvoiceItem invoiceItem)
        {
            invoice.Lines.Add(new InvoiceLine
            {
                Name = equipment.Localisations[0].Name,
                Fee = FeeCalc.CalculateFee(equipment, invoiceItem.DaysToRent),
                Bonus = FeeCalc.CalculateBonus(equipment),
                DaysToRent = invoiceItem.DaysToRent
            });
        }
    }
}
