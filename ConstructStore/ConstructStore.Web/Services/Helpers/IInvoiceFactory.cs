using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services.Helpers
{
    public interface IInvoiceFactory
    {
        InvoiceData CreateEmptyInvoice(GenerateInvoiceRequest request);
        void AppendInvoiceLine(InvoiceData invoice, Equipment equipment, InvoiceItem invoiceItem);
    }
}