using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Mapping;

namespace ConstructStore.Web.Services.Impl
{
    public class LookupServiceImpl : ILookupService
    {
        public IEquipmentTypeDao EquipmentTypeDao { get; set; }
        public ILookupMapper LookupMapper { get; set; }

        public async Task<IList<EquipmentTypeDto>> GetEquipmentTypes()
        {
            var items = await EquipmentTypeDao.GetAll();
            return LookupMapper.ToDto(items);
        }
    }
}
