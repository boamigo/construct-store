using System.Threading.Tasks;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Helpers;

namespace ConstructStore.Web.Services.Impl
{
    public class InvoiceServiceImpl: IInvoiceService
    {
        public IEquipmentDao EquipmentDao { get; set; }
        public IInvoiceFactory InvoiceFactory { get; set; }

        public async Task<InvoiceData> GenerateInvoice(GenerateInvoiceRequest request)
        {
            var invoice = InvoiceFactory.CreateEmptyInvoice(request);
            foreach (var item in request.Items)
            {
                var equipment = await EquipmentDao.GetByCode(item.EquipmentCode, request.Lang);
                InvoiceFactory.AppendInvoiceLine(invoice, equipment, item);
            }

            return invoice;
        }
    }
}
