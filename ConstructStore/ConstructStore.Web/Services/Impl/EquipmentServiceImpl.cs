using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services.Mapping;

namespace ConstructStore.Web.Services.Impl
{
    public class EquipmentServiceImpl: IEquipmentService
    {
        public IEquipmentDao EquipmentDao { get; set; }
        public IEquipmentMapper EquipmentMapper { get; set; }

        public async Task<IList<EquipmentDto>> GetAll(string lang)
        {
            var items = await EquipmentDao.GetAll(lang);
            return EquipmentMapper.ToDto(items);

        }

        public async Task<EquipmentDto> GetByCode(string code, string lang)
        {
            var item = await EquipmentDao.GetByCode(code, lang);
            return EquipmentMapper.ToDto(item);
        }
    }
}
