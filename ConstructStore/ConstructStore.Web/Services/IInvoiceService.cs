using System.Threading.Tasks;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services
{
    public interface IInvoiceService
    {
        Task<InvoiceData> GenerateInvoice(GenerateInvoiceRequest request);
    }
}
