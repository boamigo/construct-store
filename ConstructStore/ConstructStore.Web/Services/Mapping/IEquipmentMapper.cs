using System.Collections.Generic;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services.Mapping
{
    public interface IEquipmentMapper
    {
        IList<EquipmentDto> ToDto(IList<Equipment> items);
        EquipmentDto ToDto(Equipment item);
    }
}
