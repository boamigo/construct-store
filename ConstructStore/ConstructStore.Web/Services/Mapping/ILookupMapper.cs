using System.Collections.Generic;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services.Mapping
{
    public interface ILookupMapper
    {
        IList<EquipmentTypeDto> ToDto(IList<EquipmentType> items);
    }
}
