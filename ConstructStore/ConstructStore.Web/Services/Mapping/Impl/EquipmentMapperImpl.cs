using System.Collections.Generic;
using System.Linq;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;


namespace ConstructStore.Web.Services.Mapping.Impl
{
    public class EquipmentMapperImpl : IEquipmentMapper
    {
        public IList<EquipmentDto> ToDto(IList<Equipment> items)
        {
            return
                items.Select(o => new EquipmentDto
                {
                    Code = o.Code,
                    TypeCode = o.TypeCode,
                    Name = o.Localisations[0].Name
                })
                .ToList();
        }

        public EquipmentDto ToDto(Equipment item)
        {
            return new EquipmentDto
            {
                Code = item.Code,
                TypeCode = item.TypeCode,
                Name = item.Localisations[0].Name
            };
        }
    }
}
