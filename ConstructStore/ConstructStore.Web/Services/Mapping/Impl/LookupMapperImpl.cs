using System.Collections.Generic;
using System.Linq;
using ConstructStore.Web.Domain;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services.Mapping.Impl
{
    public class LookupMapperImpl : ILookupMapper
    {
        public IList<EquipmentTypeDto> ToDto(IList<EquipmentType> items)
        {
            return items.Select(o => new EquipmentTypeDto {Code = o.Code, Name = o.Name}).ToList();
        }
    }
}