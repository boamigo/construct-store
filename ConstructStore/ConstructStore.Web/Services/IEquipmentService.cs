using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services
{
    public interface IEquipmentService
    {
        Task<IList<EquipmentDto>> GetAll(string lang);
        Task<EquipmentDto> GetByCode(string code, string lang);
    }
}
