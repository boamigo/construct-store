using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Services
{
    public interface ILookupService
    {
        Task<IList<EquipmentTypeDto>> GetEquipmentTypes();
    }
}
