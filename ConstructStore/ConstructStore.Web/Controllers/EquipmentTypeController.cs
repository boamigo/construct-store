using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services;
using Microsoft.AspNetCore.Mvc;


namespace ConstructStore.Web.Controllers
{
    [Route("api/equipment-type")]
    public class EquipmentTypeController : Controller
    {
        private readonly ILookupService lookupService;

        public EquipmentTypeController(ILookupService lookupService)
        {
            this.lookupService = lookupService;
        }

        [HttpGet]
        public async Task<IList<EquipmentTypeDto>> Get()
        {
            return await lookupService.GetEquipmentTypes();
        }
    }
}
