using System.Threading.Tasks;
using ConstructStore.Web.Controllers.Helpers;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services;
using Microsoft.AspNetCore.Mvc;


namespace ConstructStore.Web.Controllers
{
    [Route("api/invoice")]
    public class InvoiceController : Controller
    {
        private readonly IInvoiceService invoiceService;
        private readonly IInvoiceTextFormatter invoiceFormatter;
        private readonly IWebResultFactory webResultFactory;
        
        public InvoiceController(
            IInvoiceService invoiceService,
            IInvoiceTextFormatter invoiceFormatter,
            IWebResultFactory webResultFactory)
        {
            this.invoiceService = invoiceService;
            this.invoiceFormatter = invoiceFormatter;
            this.webResultFactory = webResultFactory;
        }

        [HttpPost("generate")]
        public async Task<IActionResult> Generate([FromBody] GenerateInvoiceRequest request)
        {
            var invoice = await invoiceService.GenerateInvoice(request);
            var text = invoiceFormatter.FormatInvoice(invoice);
            return webResultFactory.ConvertTextToFile(text);
        }
    }
}
