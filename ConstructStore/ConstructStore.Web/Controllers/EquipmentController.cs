using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Services.Dto;
using ConstructStore.Web.Services;
using Microsoft.AspNetCore.Mvc;


namespace ConstructStore.Web.Controllers
{
    [Route("api/equipment")]
    public class EquipmentController : Controller
    {
        private readonly IEquipmentService equipmentService;

        public EquipmentController(IEquipmentService equipmentService)
        {
            this.equipmentService = equipmentService;
        }

        [HttpGet]
        public async Task<IList<EquipmentDto>> GetAll(string lang)
        {
            return await equipmentService.GetAll(lang);
        }

        [HttpGet("{code}")]
        public async Task<EquipmentDto> GetByCode(string code, string lang)
        {
            return await equipmentService.GetByCode(code, lang);
        }
    }
}
