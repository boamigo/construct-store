using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace ConstructStore.Web.Controllers.Helpers.Impl
{
    public class WebResultFactoryImpl: IWebResultFactory
    {
        public IActionResult ConvertTextToFile(string text)
        {
            return new FileContentResult(
                Encoding.UTF8.GetBytes(text),
                "text/plain");
        }
    }
}
