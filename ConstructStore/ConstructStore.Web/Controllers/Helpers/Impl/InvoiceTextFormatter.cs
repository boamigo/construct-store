using System.Text;
using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Controllers.Helpers.Impl
{
    public class InvoiceTextFormatterImpl: IInvoiceTextFormatter
    {
        public string FormatInvoice(InvoiceData invoice)
        {
            var totalFee = 0;
            var totalBonus = 0;
            var text = new StringBuilder();
            text.AppendLine(invoice.Title);
            text.AppendLine();
            foreach (var line in invoice.Lines)
            {
                totalFee += line.Fee;
                totalBonus += line.Bonus;
                text.AppendLine($"days: {line.DaysToRent}\tbonus: {line.Bonus}\tfee: {line.Fee}\t\t{line.Name}");
            }

            text.AppendLine();
            text.AppendLine($"Totals:");
            text.AppendLine($"\tFee: {totalFee}");
            text.AppendLine($"\tBonus: {totalBonus}");
            return text.ToString();
        }
    }
}