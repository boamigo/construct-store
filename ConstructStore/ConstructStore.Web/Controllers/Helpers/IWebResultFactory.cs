using Microsoft.AspNetCore.Mvc;

namespace ConstructStore.Web.Controllers.Helpers
{
    public interface IWebResultFactory
    {
        IActionResult ConvertTextToFile(string text);
    }
}
