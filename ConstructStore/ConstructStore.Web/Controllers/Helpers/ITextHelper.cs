using ConstructStore.Web.Services.Dto;

namespace ConstructStore.Web.Controllers.Helpers
{
    public interface IInvoiceTextFormatter
    {
        string FormatInvoice(InvoiceData invoice);
    }
}