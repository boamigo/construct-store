using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao.Database;
using ConstructStore.Web.Domain;


namespace ConstructStore.Web.Dao.Impl
{
    public class EquipmentDaoImpl : IEquipmentDao
    {
        public IDatabase Database { get; set; }

        public async Task<IList<Equipment>> GetAll(string lang)
        {
            return await Database.ExecuteQuery<IList<Equipment>>(QueryType.AllEquipmentsWithLang, lang);
        }

        public async Task<Equipment> GetByCode(string code, string lang)
        {
            return await Database.ExecuteQuery<Equipment>(QueryType.EquipmentByCodeWithLang, code, lang);
        }
    }
}
