using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Dao.Database;
using ConstructStore.Web.Domain;


namespace ConstructStore.Web.Dao.Impl
{
    public class EquipmentTypeDaoImpl : IEquipmentTypeDao
    {
        public IDatabase Database { get; set; }

        public async Task<IList<EquipmentType>> GetAll()
        {
            return await Database.ExecuteQuery<IList<EquipmentType>>(QueryType.AllEquipmentTypes);
        }
    }
}
