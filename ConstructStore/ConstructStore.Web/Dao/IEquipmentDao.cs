using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Domain;

namespace ConstructStore.Web.Dao
{
    public interface IEquipmentDao
    {
        Task<IList<Equipment>> GetAll(string lang);
        Task<Equipment> GetByCode(string code, string lang);
    }
}
