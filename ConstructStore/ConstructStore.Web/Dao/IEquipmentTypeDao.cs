using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Domain;

namespace ConstructStore.Web.Dao
{
    public interface IEquipmentTypeDao
    {
        Task<IList<EquipmentType>> GetAll();
    }
}
