using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConstructStore.Web.Domain;

namespace ConstructStore.Web.Dao.Database.Impl
{
    public class InMemoryDatabaseImpl : IDatabase
    {
        private IList<Equipment> equipments = new List<Equipment>
        {
            new Equipment
            {
                Code = "CTP-001",
                TypeCode = "HVY",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n
                    {
                        Name = "EN Caterpillar bulldozer",
                        LangCode = "en"
                    },
                    new EquipmentL10n
                    {
                        Name = "FR Caterpillar bulldozer",
                        LangCode = "fr"
                    },
                    new EquipmentL10n
                    {
                        Name = "DE Caterpillar bulldozer",
                        LangCode = "de"
                    }
                }
            },
            new Equipment
            {
                Code = "KMZ-001",
                TypeCode = "RGR",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n
                    {
                        Name = "EN KamAZ truck",
                        LangCode = "en"
                    },
                    new EquipmentL10n
                    {
                        Name = "FR KamAZ truck",
                        LangCode = "fr"
                    },
                    new EquipmentL10n
                    {
                        Name = "DE KamAZ truck",
                        LangCode = "de"
                    }
                }
            },
            new Equipment
            {
                Code = "KMTS-001",
                TypeCode = "HVY",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n
                    {
                        Name = "EN Komatsu crane",
                        LangCode = "en"
                    },
                    new EquipmentL10n
                    {
                        Name = "FR Komatsu crane",
                        LangCode = "fr"
                    },
                    new EquipmentL10n
                    {
                        Name = "DE Komatsu crane",
                        LangCode = "de"
                    }
                }
            },
            new Equipment
            {
                Code = "VLVO-001",
                TypeCode = "RGR",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n
                    {
                        Name = "EN Volvo steamroller",
                        LangCode = "en"
                    },
                    new EquipmentL10n
                    {
                        Name = "FR Volvo steamroller",
                        LangCode = "fr"
                    },
                    new EquipmentL10n
                    {
                        Name = "DE Volvo steamroller",
                        LangCode = "de"
                    }
                }
            },
            new Equipment
            {
                Code = "BOSCH-001",
                TypeCode = "SPZ",
                Localisations = new List<EquipmentL10n>
                {
                    new EquipmentL10n
                    {
                        Name = "EN Bosch jackhammer",
                        LangCode = "en"
                    },
                    new EquipmentL10n
                    {
                        Name = "FR Bosch jackhammer",
                        LangCode = "fr"
                    },
                    new EquipmentL10n
                    {
                        Name = "DE Bosch jackhammer",
                        LangCode = "de"
                    }
                }
            }
        };

        public async Task<T> ExecuteQuery<T>(QueryType query, params object[] paramArray)
        {
            // Emulate non-blocking database call
            return await Task.Factory.StartNew<T>(() =>
            {
                switch (query)
                {
                    case QueryType.AllEquipmentTypes:
                        return (T) (object) ExecuteGetAllEquipmentTypesQuery();
                    case QueryType.AllEquipmentsWithLang:
                        return (T) (object) ExecuteGetAllEquipmentsWithLanguageQuery((string)paramArray[0]);
                    case QueryType.EquipmentByCodeWithLang:
                        return (T) (object) ExecuteGetEquipmentByCodeWithLanguageQuery((string)paramArray[0], (string)paramArray[1]);
                }
                
                throw new Exception("Query wrong");
            });
        }

        private IList<EquipmentType> ExecuteGetAllEquipmentTypesQuery()
        {
            return new List<EquipmentType>
            {
                new EquipmentType
                {
                    Code = "HVY",
                    Name = "Heavy"
                },
                new EquipmentType
                {
                    Code = "RGR",
                    Name = "Regular"
                },
                new EquipmentType
                {
                    Code = "SPZ",
                    Name = "Specialized"
                }
            };
        }

        private IList<Equipment> ExecuteGetAllEquipmentsWithLanguageQuery(string lang)
        {
            return equipments.Select(o => new Equipment
                {
                    Code = o.Code,
                    TypeCode = o.TypeCode,
                    Localisations = o.Localisations.Where(x => x.LangCode == lang).ToList()
                })
                .ToList();
        }

        private Equipment ExecuteGetEquipmentByCodeWithLanguageQuery(string code, string lang)
        {
            var item = equipments.First(o => o.Code == code);
            return new Equipment
            {
                Code = item.Code,
                TypeCode = item.TypeCode,
                Localisations = item.Localisations.Where(x => x.LangCode == lang).ToList()
            };
        }
    }
}
