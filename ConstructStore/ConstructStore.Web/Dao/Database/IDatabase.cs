using System.Collections.Generic;
using System.Threading.Tasks;
using ConstructStore.Web.Domain;

namespace ConstructStore.Web.Dao.Database
{
    public interface IDatabase
    {
        Task<T> ExecuteQuery<T>(QueryType query, params object[] paramArray);
    }

    public enum QueryType
    {
        AllEquipmentsWithLang,
        EquipmentByCodeWithLang,
        AllEquipmentTypes
    }
}
