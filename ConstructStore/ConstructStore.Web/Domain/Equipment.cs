using System.Collections.Generic;

namespace ConstructStore.Web.Domain
{
    public class Equipment
    {
        public string Code { get; set; }
        public string TypeCode { get; set; }
        public IList<EquipmentL10n> Localisations { get; set; }
    }
}
