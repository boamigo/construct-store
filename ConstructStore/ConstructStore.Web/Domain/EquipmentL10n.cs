namespace ConstructStore.Web.Domain
{
    public class EquipmentL10n
    {
        public string Name { get; set; }
        public string LangCode { get; set; }
    }
}
