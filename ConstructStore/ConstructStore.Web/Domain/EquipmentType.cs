namespace ConstructStore.Web.Domain
{
    public class EquipmentType
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
