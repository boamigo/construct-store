﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using ConstructStore.Web.Controllers.Helpers;
using ConstructStore.Web.Controllers.Helpers.Impl;
using ConstructStore.Web.Dao;
using ConstructStore.Web.Dao.Database;
using ConstructStore.Web.Dao.Database.Impl;
using ConstructStore.Web.Dao.Impl;
using ConstructStore.Web.Services;
using ConstructStore.Web.Services.Helpers;
using ConstructStore.Web.Services.Helpers.Impl;
using ConstructStore.Web.Services.Impl;
using ConstructStore.Web.Services.Mapping;
using ConstructStore.Web.Services.Mapping.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace ConstructStore.Web
{
    public class Startup
    {
        public IContainer ApplicationContainer {get; private set;}
        
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddControllersAsServices();
            var builder = new ContainerBuilder();
            builder.Populate(services);

            // Database
            builder.RegisterType<InMemoryDatabaseImpl>().As<IDatabase>().SingleInstance().PropertiesAutowired();

            // Dao
            builder.RegisterType<EquipmentTypeDaoImpl>().As<IEquipmentTypeDao>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<EquipmentDaoImpl>().As<IEquipmentDao>().SingleInstance().PropertiesAutowired();

            // Services
            builder.RegisterType<FeeCalcImpl>().As<IFeeCalc>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<InvoiceFactoryImpl>().As<IInvoiceFactory>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<LookupMapperImpl>().As<ILookupMapper>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<LookupServiceImpl>().As<ILookupService>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<EquipmentMapperImpl>().As<IEquipmentMapper>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<EquipmentServiceImpl>().As<IEquipmentService>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<InvoiceServiceImpl>().As<IInvoiceService>().SingleInstance().PropertiesAutowired();
            
            // Controllers
            builder.RegisterType<InvoiceTextFormatterImpl>().As<IInvoiceTextFormatter>().SingleInstance().PropertiesAutowired();
            builder.RegisterType<WebResultFactoryImpl>().As<IWebResultFactory>().SingleInstance().PropertiesAutowired();
            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
