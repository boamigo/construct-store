import { EquipmentTypeDto } from '@/dto/EquipmentTypeDto';

export interface Equipment {
	code: string;
	type: EquipmentTypeDto;
	name: string;
	description: string;
}
