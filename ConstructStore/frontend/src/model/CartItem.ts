import { Equipment } from './Equipment';


export class CartItem {
	item: Equipment;
	daysToRent: number;
	fee: number;
	points: number;
}
