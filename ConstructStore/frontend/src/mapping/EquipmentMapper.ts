import { EquipmentDto } from '@/dto/EquipmentDto';
import { Equipment } from '@/model/Equipment';
import { EquipmentTypeDto } from '@/dto/EquipmentTypeDto';

export class EquipmentMapper {

	toListModel(items: EquipmentDto[], equipmentTypes: EquipmentTypeDto[]): Equipment[] {
		let models: Equipment[] = [];
		for(let item of items) {
			models.push(this.toModel(item, equipmentTypes));
		}

		return models;
	}

	toModel(item: EquipmentDto, equipmentTypes: EquipmentTypeDto[]): Equipment {
		return {
			code: item.code,
			name: item.name,
			description: item.description,
			type: equipmentTypes.find(o => o.code === item.typeCode)
		};
	}
}
