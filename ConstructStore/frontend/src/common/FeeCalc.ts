import { Equipment } from '@/model/Equipment';

const ONETIME_FEE = 100;
const PREMIUM_FEE = 60;
const REGULAR_FEE = 40;

export class FeeCalc {

	calculateFee(item: Equipment, daysToRent: number): number {
		if (item.type.code === 'HVY') {
			return ONETIME_FEE + PREMIUM_FEE * daysToRent;
		}
		else if (item.type.code === 'RGR') {
			if (daysToRent <= 2) {
				return ONETIME_FEE + PREMIUM_FEE * daysToRent;
			}
			else {
				return ONETIME_FEE + PREMIUM_FEE * 2 + REGULAR_FEE * (daysToRent - 2);
			}
		}
		else if (item.type.code === 'SPZ') {
			if (daysToRent <= 3) {
				return PREMIUM_FEE * daysToRent;
			}
			else {
				return PREMIUM_FEE * 3 + REGULAR_FEE * (daysToRent - 3);
			}
		}
	}

	calculatePoints(item: Equipment): number {
		if (item.type.code === 'HVY') {
			return 2;
		}
		else {
			return 1;
		}
	}
}
