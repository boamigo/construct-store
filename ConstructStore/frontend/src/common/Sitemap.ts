import { Vue } from 'vue-property-decorator';

export class Sitemap {
	appRoot!: Vue;
	pageNum = 1;

	constructor() {
	}

	setRootComponent(appRoot: Vue): void {
		this.appRoot = appRoot;
	}

	getPath(): string {
		return this.appRoot.$route.path;
	}

	queryString(): any {
		return this.appRoot.$route.query;
	}

	getParams(): any {
		return this.appRoot.$route.params;
	}

	gotoHome(): void {
		this.appRoot.$router.push('/home/index');
	}

	gotoSearch(): void {
		this.appRoot.$router.push('/home/search');
	}

	gotoReportAdd(): void {
		this.appRoot.$router.push('/report/add');
	}

	gotoReportServer(): void {
		this.appRoot.$router.push('/report/server');
	}
}
