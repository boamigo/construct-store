import store from '@/store';
import { FeeCalc } from '@/common/FeeCalc';
import { Equipment } from '@/model/Equipment';
import { CartItem } from '@/model/CartItem';


export class Cart {

	constructor(public feeCalc: FeeCalc) {
	}

	getItems(): CartItem[] {
		return store.state.cartItems;
	}

	addToCart(equipment: Equipment, daysToRent: number): void {
		let items: CartItem[] = store.state.cartItems;
		let existingItem = items.find(o => o.item.code === equipment.code);
		if (existingItem) {
			existingItem.daysToRent += daysToRent;
			existingItem.fee = this.feeCalc.calculateFee(equipment, existingItem.daysToRent)
			existingItem.points = this.feeCalc.calculatePoints(equipment);
			return;
		}
		else {
			items.push({
				item: equipment,
				daysToRent: daysToRent,
				fee: this.feeCalc.calculateFee(equipment, daysToRent),
				points: this.feeCalc.calculatePoints(equipment)
			});
		}

		store.commit('setCartItems', items);
	}

	removeItem(item: CartItem): void {
		let items = store.state.cartItems;
		let index = items.indexOf(item);
		items.splice(index, 1)
		store.commit('setCartItems', items);
	}
}
