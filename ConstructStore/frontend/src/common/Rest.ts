import * as axios from 'axios';

export class Rest {

	get<T>(action: string): Promise<T> {
		return axios.default.get('/api/' + action)
			.then(response => <T>response.data);
	}

	post<T>(action: string, data: any): Promise<T> {
		return axios.default.post('/api/' + action, data)
			.then(response => <T>response.data);
	}

	postAndDownloadFile(action: string, data: any, fileName: string): Promise<void> {
		return this.post(action, data)
			.then((data: string) => {
				var blob = new Blob([data]);
				var link = document.createElement('a');
				link.href = window.URL.createObjectURL(blob);
				link.download = fileName;
				link.click();
			});
	}
}
