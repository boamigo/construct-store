import store from '@/store';

export class Session {

	get showLangDialog() {
		return store.state.showLangDialog;
	}
	set showLangDialog(show: boolean) {
		store.commit('setShowLangDialog', show);
	}

	get showBackdrop() {
		return store.state.backdrop;
	}
	set showBackdrop(show: boolean) {
		store.commit('setBackdrop', show);
	}
}
