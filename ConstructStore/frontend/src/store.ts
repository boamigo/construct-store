import Vue from 'vue';
import Vuex from 'vuex';
import { CartItem } from './model/CartItem';


Vue.use(Vuex);
/*
class AppStore {
	state: StateData = new StateData();
	actions: any = {};
	mutations: any = {
		setShowLangDialog(state: any, value: boolean): void {
			state.showLangDialog = value;
		},
		setBackdrop(state: any, value: boolean): void {
			state.backdrop = value;
		},
		setCartItems(state: any, value: CartItem[]): void {
			state.cartItems = value;
		},
		setCartTotal(state: any, value: number): void {
			state.cartTotal = value;
		}
	}
}

class StateData {
	backdrop: boolean;
	showLangDialog: boolean;
	cartItems: CartItem[] = [];
	cartTotal: 0
}

export default new Vuex.Store(new AppStore());

*/
export default new Vuex.Store({
	state: {
		backdrop: false,
		showLangDialog: false,
		cartItems: [],
		cartTotal: 0
	},
	actions: {
	},
	mutations: {
		setShowLangDialog(state: any, value: boolean): void {
			state.showLangDialog = value;
		},
		setBackdrop(state: any, value: boolean): void {
			state.backdrop = value;
		},
		setCartItems(state: any, value: CartItem[]): void {
			state.cartItems = value;
		},
		setCartTotal(state: any, value: number): void {
			state.cartTotal = value;
		}
	}
});