import { Rest } from '@/common/Rest';
import { GenerateInvoiceRequest } from '@/dto/GenerateInvoiceRequest';

export class InvoiceService {

	constructor(public rest: Rest) {
	}

	generateInvoice(request: GenerateInvoiceRequest): void {
		this.rest.postAndDownloadFile('invoice/generate', request, 'invoice.txt');
	}
}
