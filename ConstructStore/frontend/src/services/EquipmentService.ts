import { Rest } from '@/common/Rest';
import { EquipmentDto } from '@/dto/EquipmentDto';

export class EquipmentService {

	constructor(public rest: Rest) {
	}

	getAll(lang: string): Promise<EquipmentDto[]> {
		return this.rest.get<EquipmentDto[]>('equipment?lang=' + lang);
	}

	getByCode(code: string, lang): Promise<EquipmentDto> {
		return this.rest.get<EquipmentDto>('equipment/:code?lang=' + lang);
	}
}
