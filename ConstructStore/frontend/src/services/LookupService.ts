import { Rest } from '@/common/Rest';
import { EquipmentTypeDto } from '@/dto/EquipmentTypeDto';


export class LookupService {

	constructor(public rest: Rest) {
	}

	getEquipmentTypes(): Promise<EquipmentTypeDto[]> {
		return this.rest.get<EquipmentTypeDto[]>('equipment-type');
	}
}
