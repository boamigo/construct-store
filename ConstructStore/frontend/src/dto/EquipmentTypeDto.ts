export interface EquipmentTypeDto {
	code: string;
	name: string;
}
