export interface EquipmentDto {
	code: string;
	name: string;
	description: string;
	typeCode: string;
}
