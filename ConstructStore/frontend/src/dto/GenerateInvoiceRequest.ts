export class GenerateInvoiceRequest {
	lang: string;
	items: {
		equipmentCode: string;
		daysToRent: number;
	}[];
}
